FROM certbot/certbot
LABEL Author="Vyacheslav Voronchuk <voronchuk@gmail.com>"

ENV KUBE_LATEST_VERSION="v1.17.4"

RUN apk add --update ca-certificates bash \
 && apk add --update -t deps curl \
 && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl \
 && apk del --purge deps \
 && rm /var/cache/apk/*

COPY ./run.sh /var/app/run.sh
RUN chmod +x /var/app/run.sh
COPY ./run_base64.sh /var/app/run_base64.sh
RUN chmod +x /var/app/run_base64.sh
WORKDIR /var/app

RUN crontab -l | { cat; echo "15 2 12 * * certbot renew"; } | crontab -

EXPOSE 80
ENTRYPOINT []
CMD ["/bin/sleep", "infinity"]
#CMD ["/bin/bash", "-c", "/var/app/run.sh"]